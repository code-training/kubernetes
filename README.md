# kubernetes

## Commands

### Watch all cluster

> watch kubectl get all

### NAMESPACES

#### Get Namespaces

> kubectl get ns

### PODS

### Delete pod

> kubectl delete pod hello-world

### See Logs pod

> kubectl logs hello-world-s8hrn

### Go inside to pods

> kubectl -n test exec -it [pod_name] bash

### DEPLOYMENTS

### NODES

#### Get nodes

> kubectl get nodes

#### Get all nodes wide

> kubectl -n test get nodes -o wide

### SERVICES

#### Get services

> kubectl -n test get svc

#### Get all services by label

> kubectl -n test get services -l app=nginx-app

#### Describe services

> kubctl -n test describe svc nginx-service

### JOBS

#### Get jobs

> kubectl get job

#### Create job

> kubectl create -f file.yml

#### Delete job

> kubectl delete job hello-world

### See description of job

> kubectl describe job hello-world | less

### TEST

#### Using curl

> curl http://$(minikube ip):32000

# Tutorials

- [Kubernetes Made Easy | Kubernetes Tutorial](https://www.youtube.com/watch?v=eth7osiCryc&list=PLMPZQTftRCS8Pp4wiiUruly5ODScvAwcQ&index=1)

- [Learn kubernetes](https://www.youtube.com/watch?v=YzaYqxW0wGs&list=PL34sAs7_26wNBRWM6BDhnonoA5FMERax0)

# JOB + QUEUE
https://kubernetes.io/docs/tasks/job/fine-parallel-processing-work-queue/
