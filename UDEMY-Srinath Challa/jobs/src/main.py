import os

if __name__ == "__main__":
  DATE_START = os.environ.get('DATE_START', None)
  print(f"Date to start {DATE_START}")
  DATE_END = os.environ.get('DATE_END', None)
  print(f"Date to end {DATE_END}")
