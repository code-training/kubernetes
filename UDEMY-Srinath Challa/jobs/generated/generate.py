import yaml
import json
import uuid
import os


def get_yml_template(file_path):
  with open(file_path) as file:
    return yaml.load(file, Loader=yaml.FullLoader)

def generate_file(idx, date, options = []):
  envs = [{
    'name': 'DATE_START',
    'value': date
  },{
    'name': 'DATE_END',
    'value': date
  }]
  #print(envs + options)
  job_name = f'seace-job-{str(idx)}'
  yml = get_yml_template(r'./template.yml')
  yml['metadata']['name'] =  job_name
  yml['spec']['template']['spec']['containers'][0]['env'] = envs + options
  file_name = os.path.join('jobs', f'{str(uuid.uuid1())}.yml')
  with open(file_name, 'w') as file:
      yaml.dump(yml, file)
  print(file_name)



def get_dates():
  return ['20/03/2020','21/03/2020']


def main():

  dates = get_dates()
  options = [{
    'name':'NUMBER_BROWSER',
    'value': "1"
  }]

  for idx, date in enumerate(dates):
    generate_file(idx, date, options)

if __name__ == "__main__":
    main()
